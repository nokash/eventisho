import React, { Component } from 'react';
import {
    Collapse,
    navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    Container,
    Navbar
} from 'reactstrap';

class AppNavbar extends Component{
    state = {
        isOpen: false
    }
   
    toggle = () => {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render() {
       return(
        <div>
        <Navbar color="dark" dark expand="sm" className="mb-5">
            <Container>
                <NavbarBrand href="/">Eventisho</NavbarBrand>
                <NavbarToggler onClick={this.toggle}/>
                <Collapse isOpen={this.state.isOpen} navbar>
                    <Nav className="ml-auto" navbar>
                        <NavItem>
                            <NavLink href="/">GitHub</NavLink>
                        </NavItem>
                    </Nav>
                </Collapse>
            </Container>
        </Navbar>
    </div>
       )
    }
}



export default AppNavbar;