const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const items = require('./routes/api/items')

const app = express();

//Coonnect to MongoDB
// mongoose.connect('mongodb://localhost/testaroo');

// mongoose.connection.once('open', function(){
//     console.log("Connect has been made!");
// }).on('error', function(error){
//     console.log("Connection Error: ", error);
// });

// Bodyparser Middleware
app.use(bodyParser.json());

//DB Config
const db = require('./config/keys').mongoURI;

//Connect to Mongo
mongoose.connect(db)
    .then(() => console.log("MongoDB connected..."))
    .catch(err => console.log(err));

//Use Routes
app.use('/api/items', items);

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server started on port: ${port}`));
